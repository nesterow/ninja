import Parse from './parse';

export default class User extends Parse.User {

  constructor(fields) {
    super();
    for (let k in fields){
      this.set(k, fields[k]);
    }
  }

}
