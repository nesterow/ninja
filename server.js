require('dotenv').config();

const {
    NODE_ENV,
    MONGO_DB_URL,
    APP_ID,
    APP_MASTER_KEY,
    APP_JS_KEY,
    APP_HOST,
    APP_PORT,
    APP_API_URI,
    APP_DASHBOARD_URI,
    APP_DASHBOARD_USERNAME,
    APP_DASHBOARD_PASSWORD
} = process.env;

const { application } = require('./config.json');

const express = require('express');
const body_parser = require('body-parser');
const cookie_parser = require('cookie-parser');
const next = require('next');

const dev = NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const i18n_plugin = require('./plugins/i18n/plugin');

const ParseServer = require('parse-server').ParseServer;
const ParseDashboard = require('parse-dashboard');



const api = new ParseServer({
    databaseURI: MONGO_DB_URL,
    cloud: './handlers',
    appId: APP_ID,
    masterKey: APP_MASTER_KEY,
    javascriptKey: APP_JS_KEY,
    allowClientClassCreation: false
});

const dashboard = new ParseDashboard({
    "apps": [
        {
            "serverURL": `${ dev ? 'http' : 'https' }://${APP_HOST}:${APP_PORT}${APP_API_URI}`,
            "appId": APP_ID,
            "masterKey": APP_MASTER_KEY,
            "appName": application.app_name
        }
    ],
    "users": [
        {
            "user": APP_DASHBOARD_USERNAME,
            "pass": APP_DASHBOARD_PASSWORD,
            "apps": [{"appId": APP_ID}]
        }
    ]
});


app.prepare().then(async ()=>{

    const server = express();

    server.use(cookie_parser());
    server.use(body_parser.json());
    server.use(body_parser.urlencoded({extended:true}));
    server.use(APP_API_URI, api);
    server.use(APP_DASHBOARD_URI, dashboard);

    await i18n_plugin(server);

    server.get('*', (req, res) => {
        return handle(req, res);
    });



    server.listen(APP_PORT, (err) => {
        if (err) throw err;
        console.log('> Ready on http://localhost:3000')
    })


})
.catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
});
