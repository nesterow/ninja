import { withI18next } from '../../plugins/i18n/withI18next';
// compose a react component with modifiers
// add your modifier in the chain
export default (Component) => {

  let mods = [
    {
      name: 'i18n',
      modify: function (Component) {
        return withI18next(Component[this.name])(Component)
      }
    }
  ];

  let compose = () => {
      let result = Component;
      for (let i in mods) {
        result = mods[i].modify(Component)
      }
      return result;
  };

  return compose();
}
