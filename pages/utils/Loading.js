import { Component } from 'react';

export default class Loader extends Component {

    constructor(){
        super();
        this.state = {};
    }

    close(){
        this.setState({
            visibility: 'hidden',
            opacity: 0.01,
            transition: 'visibility 0s 1s,opacity 1s ease-out',
        });

        setTimeout(()=>this.setState({destroyed: true}),1100);
    }

    open(){
		setTimeout(()=>this.setState({destroyed: false}),100);
	}

    render() {
        return ( !this.state.destroyed &&
            <div style={{
                position: 'fixed',
                zIndex:1000,
                top: 0,
                left: 0,
                width: '100%',
                height: '100vh',
                background: '#333',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                opacity: this.state.opacity || 1,
                visibility: this.state.visibility || 'visible',
                transition: this.state.transition
            }}>
				<svg width="100" height="100" viewBox="0 0 45 45" xmlns="http://www.w3.org/2000/svg" stroke="#fff" style={{width: 100}}>
					<g fill="none" fillRule="evenodd" transform="translate(1 1)" strokeWidth="2">
						<circle cx="22" cy="22" r="6" strokeOpacity="0">
							<animate attributeName="r"
									 begin="1.5s" dur="3s"
									 values="6;22"
									 calcMode="linear"
									 repeatCount="indefinite" />
							<animate attributeName="stroke-opacity"
									 begin="1.5s" dur="3s"
									 values="1;0" calcMode="linear"
									 repeatCount="indefinite" />
							<animate attributeName="stroke-width"
									 begin="1.5s" dur="3s"
									 values="2;0" calcMode="linear"
									 repeatCount="indefinite" />
						</circle>
						<circle cx="22" cy="22" r="6" strokeOpacity="0">
							<animate attributeName="r"
									 begin="3s" dur="3s"
									 values="6;22"
									 calcMode="linear"
									 repeatCount="indefinite" />
							<animate attributeName="stroke-opacity"
									 begin="3s" dur="3s"
									 values="1;0" calcMode="linear"
									 repeatCount="indefinite" />
							<animate attributeName="stroke-width"
									 begin="3s" dur="3s"
									 values="2;0" calcMode="linear"
									 repeatCount="indefinite" />
						</circle>
						<circle cx="22" cy="22" r="8">
							<animate attributeName="r"
									 begin="0s" dur="1.5s"
									 values="6;1;2;3;4;5;6"
									 calcMode="linear"
									 repeatCount="indefinite" />
						</circle>
					</g>
				</svg>

            </div>
        )
    }
}