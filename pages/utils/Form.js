import { Component } from 'react'

class Form extends Component {

  constructor(){
    super();
    this.state = {};
  }

  reset() {
    const keys = Object.keys(this.state);
    const stateReset = keys.reduce((acc, v) => ({ ...acc, [v]: undefined }), {});
    this.setState({ ...stateReset, ...this.initialState });
  }

  getError(res){
    let code = res.code;
    let message = (res.message || '')
      .toUpperCase()
      .split(' ').join('_')
      .split('.').join('')
      .split(',').join('_')
      .split(':').join('_')
      .split('"').join('_')
      .split('\'').join('_')
      .split('!').join('')
      .split('/').join('_')
      ;
    let key = `${code}_${message || 'ERROR'}`
    return this.props.t(key);
  }

  setError(res){
    if (this.errors && this.errors[res.code])
    {
      let state = {input_error:true};

      let field = this.errors[res.code]
      if (typeof(field) === 'function')
        field = field();

      state[field + '_error'] = this.props.t(this.getError(res));
      this.reset();
      this.setState(state);
    }

    else
    {
      this.reset();
      this.setState({ error: this.getError(res) });
    }
  }

}

export default Form;
