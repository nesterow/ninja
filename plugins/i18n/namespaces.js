const fs = require('fs');

function get_namespaces(path) {
  const ns = [];
  fs.readdirSync(path).forEach(file => {
    ns.push(file.replace('.json', ''));
  });
  return ns;
}

module.exports = get_namespaces;
