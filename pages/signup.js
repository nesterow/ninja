
import FormBase from './utils/Form';
import compose from './utils/compose';

import User from '../api/user';
import Router from 'next/router';
import Link from 'next/link';
import Layout from './layout';
import { Form, Input, Button, Message } from 'semantic-ui-react'
import './styles/signin.css'



class Signup extends FormBase {

  static i18n = ['signup.form'];


  constructor(){
    super();

    this.errors = {
      '-1': () => {

        if (!this.form.username)
          return 'username';

        if(!this.form.email)
          return 'email';

        if (!this.form.password)
          return 'password';

      },

      125: 'email',
      203: 'email',

    };

    this.form = {
      username:'',
      email:'',
      password: ''

    };

    if (User.current())
        Router.push('/');
  }

  setName = (ev) => {
    this.form.username = ev.target.value;
    this.reset();
  };

  setEmail = (ev) => {
    this.form.email = ev.target.value;
    this.reset();
  };

  setPassword = (ev) => {
    this.form.password = ev.target.value;
    this.reset();
  };

  send  = async (ev) => {
    ev.preventDefault();

    this.setState({
      loading: true
    });
    let user = new User(this.form);
    try
    {
      let response = await user.signUp();
      this.reset();
      Router.push('/signin');
    }

    catch(e)
    {
      this.setError(e);
    }

  };

  closeErrorMessage = ()=>{
    this.reset();
  };

  getErrorMessage(t){
    return (
      this.state.error ? (
        <Message
          className="form-error"
          style={{
            height: (this.container || {}).offsetHeight - 12
          }}
        >
          <div>
            {this.state.error}
          </div>
          <div className="form-error-footer">
            <Button onClick={this.closeErrorMessage} primary>
              {t('back')}
            </Button>
          </div>
        </Message>
      ) : ''
    )
  }

  render(){
    const { t } = this.props;
    return (
    <Layout>
      <div className="ui very padded text container ">

        <div className="ui middle aligned center aligned grid">
          <div className="column">
            {this.getErrorMessage(t)}
            <Form method="post" action="/sign-up">
              <div ref={(c) => this.container = c } className="ui stacked segment">

                <Form.Field>
                  <Input icon='user' iconPosition='left'
                      type="text"
                      onChange={this.setName}
                      name="name"
                      placeholder={t('name_placeholder')}
                      autoComplete="off"
                      error={!!this.state.username_error}
                      />
                      { this.state.username_error && (
                          <div className="form-input-error">
                            {this.state.username_error}
                          </div>
                        )
                      }
                </Form.Field>

                <Form.Field>
                  <Input icon='envelope' iconPosition='left'
                      type="email"
                      onChange={this.setEmail}
                      name="email"
                      placeholder={t('email_placeholder')}
                      autoComplete="off"
                      error={!!this.state.email_error}
                      />
                      { this.state.email_error && (
                          <div className="form-input-error">
                            {this.state.email_error}
                          </div>
                        )
                      }
                </Form.Field>

                <Form.Field>
                  <Input icon='lock' iconPosition='left'
                      type="password"
                      onChange={this.setPassword}
                      name="password"
                      placeholder={t('password_placeholder')}
                      autoComplete="off"
                      error={!!this.state.password_error}
                      />
                      { this.state.password_error && (
                          <div className="form-input-error">
                            {this.state.password_error}
                          </div>
                        )
                      }
                </Form.Field>
                <Button fluid size='large' color={this.state.input_error ? 'red' : 'teal'}
                  onClick={this.send} loading={this.state.loading}>
                  {t('sign_up')}
                </Button>
              </div>

            </Form>

            <div className="ui message">
              {t('suggest_sign_in')} &nbsp;
              <Link href="/signin">
                <a>{t('sign_in')}</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Layout>
    );
  }
}
export default compose(Signup)
