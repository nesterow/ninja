import { Component } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import compose from './utils/compose';
import User from '../api/user';
import { Dropdown, Menu, Icon } from 'semantic-ui-react';

class Layout extends Component {

    signOut = async () => {
        await User.logOut();
        Router.push('/signin');
    };

    render(){
        const { t, children, title, user } = this.props;

        return (
          <section>
            <Head>
              <title>{ title || t('default_site_title') }</title>
              <meta charSet='utf-8' />
              <meta name='viewport' content='initial-scale=1.0, width=device-width' />
            </Head>

			  <Menu size='large'>

				  <Menu.Menu position='right'>
                    { user
                        ?
                        (
							<Menu.Item>
								<Icon name='user circle' />
								<Dropdown button className='icon' text={ user.getEmail() } icon='chevron down'>
									<Dropdown.Menu>
										<Dropdown.Item>
											<Icon name='user'/>
											Profile
										</Dropdown.Item>
										<Dropdown.Item onClick={this.signOut}>
											<Icon name='sign-out alternate'/>
											Sign Out
										</Dropdown.Item>
									</Dropdown.Menu>
								</Dropdown>
							</Menu.Item>

                        )
                        :
                        (
							<Menu.Item>
								<Link href="/signin">
									<a className="ui primary button">Sign In</a>
								</Link>
							</Menu.Item>
                        )
                    }

				  </Menu.Menu>
			  </Menu>

              { children }

          </section>
        )
    }

}


export default compose(Layout);
