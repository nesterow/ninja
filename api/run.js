import Parse from './parse';

export default (method) => {
    return Parse.Cloud.run(method); // Promise
}