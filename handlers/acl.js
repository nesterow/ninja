Parse.Cloud.define('setUsersAcls', function (request, response) {
    var currentUser = request.user;
    currentUser.setACL(new Parse.ACL(currentUser));
    currentUser.save(null, {
        useMasterKey: true,
        success: function (object) {
            response.success("Acls Updated");
        },
        error: function (object, error) {
            response.error("Got an error " + error.code + " -  " + error.description);
        }
    });
});