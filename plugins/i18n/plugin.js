const i18next_middleware = require('i18next-express-middleware');
const i18next_backend = require('i18next-node-fs-backend');
const config = require('../../config.json');
const i18n = require('.');
const get_i18n_namespaces = require('./namespaces');
const parseURL = require('url').parse;
const express = require('express');

//
const { join } = require('path');

module.exports = async (server) => {

  const { localesPath, allLanguages, defaultLanguage, enableSubpaths } = config.translation;

  const i18n_options = {
     fallbackLng: defaultLanguage,
     preload: allLanguages,
     ns: get_i18n_namespaces(`${localesPath}${defaultLanguage}`), // need to preload all the namespaces
     backend: {
       loadPath: join(__dirname, '..', '..', '/static/locales/{{lng}}/{{ns}}.json'),
       addPath: join(__dirname, '..', '..', '/static/locales/{{lng}}/{{ns}}.missing.json'),
     },
     detection: {
       order: ['path', 'session', 'querystring', 'cookie', 'header'], // all
       lookupPath: 'lng',
       lookupFromPathIndex: 0,
     },
     whitelist:allLanguages
  };

  await i18n
     .use(i18next_backend)
     .use(i18next_middleware.LanguageDetector)
     .init(i18n_options);


   // enable middleware for i18next
   server.use(i18next_middleware.handle(i18n));
   server.use('/locales', express.static(join(__dirname, '..','..', '/locales')));

   if (process.env.NODE_ENV !== 'production')
    server.post('/static/locales/:lng/:ns', i18next_middleware.missingKeyHandler(i18n));

  return Promise.resolve(1);
}
