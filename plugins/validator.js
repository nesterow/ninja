const validator = require('validator');
validator.tooLong = (data) => !(data.length > 64);
validator.tooShort = (data) => !(data.length < 4);
validator.isRequired = (data) => {
	if (data && data.trim())
		return true;
	return false;
};

module.exports = validator;