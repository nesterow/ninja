
import NProgress from 'nprogress';
import App, { Container } from 'next/app';
import Head from 'next/head';
import Router from 'next/router';
import { NamespacesConsumer } from 'react-i18next';
import i18n from '../plugins/i18n';
import { translation } from '../config.json';
import 'nprogress/nprogress.css';
import Loading from "./utils/Loading";
import State from './utils/State';

import User from '../api/user';

NProgress.configure({
  showSpinner: false
});

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => !State.processing && NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());


export default class MyApp extends App {

	constructor(){
        super();
        if (typeof window !== 'undefined')
        {

        	State.loading(()=>{
				this.preloader.open();
			});

        	State.onLoading(()=>{
				NProgress.start();
			});

            State.onComplete(()=>{
                this.preloader.close();
				NProgress.done();
            });

            //Start fetching data
            setTimeout(() => State.complete(), 2000 );

        }
        else
        {
            //prefetch data on server
        }
    }


    render() {
        const { Component, pageProps } = this.props;
		const user = User.current();

        return (
            <Container>
                <Head>
                  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.css" />
                </Head>
                <Loading ref={(c)=>this.preloader = c} />
                <NamespacesConsumer {...pageProps} ns="common"
						i18n={(pageProps && pageProps.i18n) || i18n}
						wait={process.browser} >

                    {t => (
                        <React.Fragment>
                          <Component {...pageProps} user={user}/>
                        </React.Fragment>
                    )}

                </NamespacesConsumer>
            </Container>
        );
    }
}
