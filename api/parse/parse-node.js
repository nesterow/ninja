import Parse from 'parse/node';
import { application } from '../../config.json'

Parse.initialize(application.app_id, application.javascript_key, process.env.APP_MASTER_KEY);
Parse.serverURL = application.serverURL;
// Parse.User.enableUnsafeCurrentUser();

export default Parse
