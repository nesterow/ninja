//Global state for pages
//Port it to Redux if needed.

module.exports = {

	processing: false,
	error: false,

	loading(cb){
		this.processing = true;
		setTimeout(cb || (()=>{}), 100);
		setTimeout(this._on_loading || (()=>{}), 100);
	},

	complete(){
		this.processing = false;
	},

	onLoading(cb){
		this._on_loading = cb;
	},

	onComplete(cb) {
		if (!this.processing)
			return cb();

		let bounce = () => {
			if (!this.processing)
				return cb();
			setTimeout(bounce, 100);
		};
		bounce();
	}

};