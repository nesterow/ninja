import Parse from 'parse';
import { application } from '../../config.json'

Parse.initialize(application.app_id, application.javascript_key);
Parse.serverURL = application.serverURL;

export default Parse
